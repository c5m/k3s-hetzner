resource "hcloud_server" "master" {
  depends_on = [
    hcloud_network_subnet.kube_subnet,
    random_string.k3s_token,
  ]

  name                = format(var.master_name_format, count.index + 1)
  server_type         = var.master_type
  image               = var.image
  location            = var.location
  placement_group_id  = hcloud_placement_group.kube_resource_group.id
  user_data           = data.template_file.master_cloud_config[count.index].rendered

  count               = var.enable_multi_master? 3: 1
}

resource "hcloud_server_network" "master" {
  server_id   = hcloud_server.master[count.index].id
  subnet_id   = hcloud_network_subnet.kube_subnet.id
  ip          = cidrhost(var.network_subnet, count.index + 1)
  count       = var.enable_multi_master? 3: 1
}

resource "null_resource" "master" {
  depends_on = [
    hcloud_server.master,
    hcloud_server_network.master,
  ]

  triggers = {
    master_id = hcloud_server.master[0].id
  }

  provisioner "local-exec" {
    command = "until ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${var.cert_private} ubuntu@${hcloud_server.master[0].ipv4_address} 'while [ ! -f /var/lib/cloud/instance/boot-finished ]; do sleep 1; done'; do sleep 1; done"
  }

  provisioner "local-exec" {
    command = "ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${var.cert_private} ubuntu@${hcloud_server.master[0].ipv4_address} cat /etc/rancher/k3s/k3s.yaml | sed -e 's/127.0.0.1/${length(hcloud_load_balancer.master) > 0? hcloud_load_balancer.master[0].ipv4 : hcloud_server.master[0].ipv4_address}/g' > /tmp/kube_config.yaml"
  }
}
