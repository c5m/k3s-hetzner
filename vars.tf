variable "hcloud_token" {
  default = ""
}

variable "key" {
  default = "key"
}

variable "cert" {
  default = "~/.ssh/id_rsa.pub"
}

variable "cert_private" {
  default = "~/.ssh/id_rsa"
}


variable "location" {
  default = "fsn1"
}

variable "image" {
  default = "ubuntu-20.04"
}

variable "k3s_channel" {
  default = "stable"
}

variable "cluster_autoscaler_version" {
  default = "v1.21.1"
}

variable "cluster_cidr" {
  default = "10.42.0.0/16"
}

variable "network_name" {
  default = "kube_network"
}

variable "master_firewall_name" {
  default = "master_firewall"
}

variable "worker_firewall_name" {
  default = "worker_firewall"
}

variable "network_range" {
  default = "172.16.0.0/12"
}

variable "network_subnet" {
  default = "172.16.1.0/24"
}

variable "placement_group_name" {
  default = "kube_placement_group"
}

variable "master_name_format" {
  default = "master-%d"
}

variable "master_type" {
  default = "cx11"
}

variable "node_name_format" {
  default= "node-%d"
}

variable "node_type" {
  default = "cx11"
}

variable "enable_multi_master" {
  default = false
}

variable "enable_cluster_autoscaler" {
  default = true
}
