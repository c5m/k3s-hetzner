terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.34"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token == "" ?  data.external.hcloud_token.result.token : var.hcloud_token
}

provider "kubectl" {
}
